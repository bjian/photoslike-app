//
//  PhotoViewController.swift
//  Photos-like App
//
//  Created by Michael Chien on 2019/5/8.
//  Copyright © 2019 Michael Chien. All rights reserved.
//

import UIKit
import Photos

class PhotoViewController: UIViewController {
    
    var myAsset:PHAsset!
    var number:Int!
    var allResults:PHFetchResult<PHAsset>?
    let formatter = DateFormatter()
    var photoDate:String!
    let title3 = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title3)
    let footnote = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.footnote)
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollView.delegate = self

        formatter.dateFormat = "yyyy年MM月dd日 a hh:mm"
        setImage()
    }
    
    @IBAction func singleTap(_ sender: UITapGestureRecognizer) {
        self.navigationController?.setNavigationBarHidden(!self.navigationController!.isNavigationBarHidden, animated: false)
        changeBgColor(tapNum:1)
    }
    
    @IBAction func doubleTap(_ sender: UITapGestureRecognizer) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        changeBgColor(tapNum:2)
        UIView.animate(withDuration: 0.5, animations: {
            if self.scrollView.zoomScale == 1.0{
                self.scrollView.zoomScale = 3.0
            }else{
                self.scrollView.zoomScale = 1.0
            }
        })
    }
    
    func changeBgColor(tapNum:Int){
        _ = tapNum
        if tapNum == 1{
            if self.view.backgroundColor == .white{
                self.view.backgroundColor = .black
            }else{
                self.view.backgroundColor = .white
            }
        }else{
            self.view.backgroundColor = .black

        }
    }
    
    @IBAction func SwipePhoto(_ sender: UISwipeGestureRecognizer) {
        switch sender.direction{
        case UISwipeGestureRecognizer.Direction.right:
            if number > 0 {
                number -= 1
            }else{
                number = 0
            }
            
        case UISwipeGestureRecognizer.Direction.left:
            if number < allResults!.count {
                number += 1
            }else{
                number = allResults!.count
            }
        default:
            break
        }
        setImage()
    }
    
    func setImage(){
        if let asset = self.allResults?[number]{
            PHImageManager.default().requestImage(for: asset, targetSize: PHImageManagerMaximumSize,contentMode: .default,options: nil, resultHandler: {(image, _: [AnyHashable : Any]?) in
                self.imageView.image = image
            })
        }
        myAsset = self.allResults![number]
        changeTitleText()
    }
    
    func changeTitleText(){
        photoDate = formatter.string(from: self.myAsset.creationDate!)
        
        PHImageManager.default().requestImageData(for: myAsset, options: nil, resultHandler: {_, _, _, info in
            //self.title = (info!["PHImageFileURLKey"] as! NSURL).lastPathComponent ?? "" + "\n\(self.myAsset.creationDate!)"
            self.titleText.text = (info!["PHImageFileURLKey"] as! NSURL).lastPathComponent! + "\n\(self.myAsset.creationDate!)"
            let s1 = NSMutableAttributedString(string: (info!["PHImageFileURLKey"] as! NSURL).lastPathComponent!, attributes: [NSMutableAttributedString.Key.font: self.title3])
            let s2 = NSMutableAttributedString(string: "\n\(self.photoDate!)", attributes: [NSMutableAttributedString.Key.font : self.footnote])
            s1.append(s2)
            self.titleText.textAlignment = .center
            self.titleText.attributedText = s1
        })
    }
    
    @IBAction func actionClick(_ sender: UIBarButtonItem) {
        let image = imageView.image
        let vc = UIActivityViewController(activityItems: [image!], applicationActivities: nil)
        present(vc, animated: true, completion: nil)
    }
    
}

extension PhotoViewController: UIScrollViewDelegate{
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        var centerX = scrollView.center.x
        var centerY = scrollView.center.y
        centerX = scrollView.contentSize.width > scrollView.frame.size.width ?
            scrollView.contentSize.width/2:centerX
        centerY = scrollView.contentSize.height > scrollView.frame.size.height ?
            scrollView.contentSize.height/2:centerY
        imageView.center = CGPoint(x: centerX, y: centerY)
    }
}

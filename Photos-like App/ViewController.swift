//
//  ViewController.swift
//  Photos-like App
//
//  Created by Michael Chien on 2019/5/5.
//  Copyright © 2019 Michael Chien. All rights reserved.
//

import UIKit
import Photos

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{
    
    var assetsFetchResults:PHFetchResult<PHAsset>?
    var assetGridThumbnailSize:CGSize!
    var imageManager:PHCachingImageManager!
    var myAsset:PHAsset!
    var Photonumber:Int!
    
    var fullScreenSize :CGSize!
    @IBOutlet weak var photoCollectionView: UICollectionView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fullScreenSize =
            UIScreen.main.bounds.size
        assetGridThumbnailSize = CGSize(width: CGFloat(fullScreenSize.width)/4 - 10.0, height: CGFloat(fullScreenSize.height)/4 - 10.0)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        PHPhotoLibrary.requestAuthorization({(status) in
            if status != .authorized{
                return
            }
            let allPhotoOptions = PHFetchOptions()
            allPhotoOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
            
            allPhotoOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
            self.assetsFetchResults = PHAsset.fetchAssets(with: PHAssetMediaType.image
                , options: allPhotoOptions)
            
            self.imageManager = PHCachingImageManager()
            
            self.resetCachedAssets()
            
            DispatchQueue.main.async {
                self.photoCollectionView.reloadData()
            }
        })
    }
    
    func resetCachedAssets(){
        self.imageManager.stopCachingImagesForAllAssets()
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.assetsFetchResults?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identify:String = "ViewCell"
        let cell = (self.photoCollectionView.dequeueReusableCell(withReuseIdentifier: identify, for: indexPath)) as UICollectionViewCell
        
        if let asset = self.assetsFetchResults?[indexPath.row]{
            self.imageManager.requestImage(for: asset, targetSize: assetGridThumbnailSize,contentMode: PHImageContentMode.aspectFill,options: nil) {(image, nfo) in
                    (cell.contentView.viewWithTag(1) as! UIImageView).image = image
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        myAsset = self.assetsFetchResults![indexPath.row]
        Photonumber = indexPath.row
        performSegue(withIdentifier: "gotoScalePhoto", sender: myAsset)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoScalePhoto"{
            let vc = segue.destination as! PhotoViewController
            vc.myAsset = myAsset!
            vc.number = Photonumber!
            vc.allResults = assetsFetchResults!
        }
    }

}

